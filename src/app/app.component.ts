import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  //template:'<h1>hi its me</h1>',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'hotelinventory';
  role = 'users   ';
}
