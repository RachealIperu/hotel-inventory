import {Component, Input, OnInit} from '@angular/core';
import {Room, RoomList} from "../rooms";
import {RoomsListComponent} from "./rooms-list/rooms-list.component";

@Component({
  selector: 'app-rooms',
  templateUrl: './rooms.component.html',
  styleUrls: ['./rooms.component.css']
})
export class RoomsComponent implements OnInit {
  constructor() { }
  roomList:RoomList[]=[];

  ngOnInit(): void {
this.roomList = [{
  roomNumber:1,
  roomType:"5 star",
  amenities:"wifi,air conditioner,lunch",
  price:20000,
  photos:"https://unsplash.com/photos/UN6LJBFz5UU",
  checkInTime:new Date('12-sept-2022'),
  checkOutTime:new Date('13-sept-2022'),
},
  {
    roomNumber:2,
    roomType:"Deluxe",
    amenities:"wifi,air conditioner,lunch",
    price:500,
    photos:"https://unsplash.com/photos/UN6LJBFz5UU",
    checkInTime:new Date('12-Sept-2022'),
    checkOutTime:new Date('13-sept-2022'),
  },
  {
    roomNumber:3,
    roomType:"private",
    amenities:"wifi,air conditioner,lunch",
    price:20000,
    photos:"https://unsplash.com/photos/UN6LJBFz5UU",
    checkInTime:new Date('12-Sept-2022'),
    checkOutTime:new Date('13-sept-2022'),
  }]
  }


  hotelName = 'Grand imperial';
  numberOfRooms = 15;
  hideRooms = false;
  toggle():void{
    this.hideRooms=!this.hideRooms;
  }



  // implementing my interface
  rooms: Room = {
    totalNumberOfRooms: 20,
    availableRooms: 10,
    bookedRooms: 10,
  }
  selectRoom(room:RoomList){
    console.log(room);
  }

}
